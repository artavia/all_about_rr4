# Description
An expanded demo based on lesson called &quot;All About React Router 4&quot; found at css-tricks.com.

## Quick word about babel-presets
During compilation I was warned about **env** and to visit the [babel homepage](http://babeljs.io/env "link to env advisory") to read further on the subject. Although, I did not utilize **babel-preset-es2015** specifically, it is safe to say that **babel-preset-env** is going to be shelved in favor of **&#64;babel/preset-env**. You can see the changes in the commit history if you desire. Also, you may as well go for the gusto in this case. In the short run, you would run something to this effect to swap out one package for the other like so:
```sh
$ npm uninstall babel-preset-env babel-core babel-loader babel-preset-react --save-dev
$ npm install @babel/preset-env @babel/core "babel-loader@^8.0.0-beta" @babel/preset-react --save-dev
```

## Please visit
You can see [the lesson](https://artavia.gitlab.io/all_about_rr4/dist/index.html "the lesson at github") and decide if this is something for you to play around with at a later date. 

## Historical background
Again, this is something I slapped together to help me learn the basic use of properly passing parameters with React Routes. The article was originally found at [css-tricks.com](https://css-tricks.com/react-router-4/ "link to article entitled All About React Router 4").

## If you are short on time like I am&hellip;
&hellip;then this project can possibly help you comprehend the lesson in stages. There are several different commented out Webpack bundle entry points that are incremented gradually as the lesson progresses along. 

## My Personal Advice
Find and compose a Webpack and/or ReactJS development environment that is comfortable to you and stick with it. As you move from one project to another, you can always add or subtract packages that are necessary for development and production of your own project. Again, this was just a learning aid that I put together in order to help myself organize and present the deluge of information that came at me at one time in a more linear fashion for future reference. I spiffed it up a little in some spots with some very basic Bootstrap classes&#45; nothing terribly fancy. Kudos belong to the author, Mr. Westfall. Thank you, Brad! I learned a lot from you and you are a swell guy! =) PEACE.

### My name is Luis
You can call me don Lucho &amp; I hope you have fun during the rest of your day building something cool to share with the rest of the world!