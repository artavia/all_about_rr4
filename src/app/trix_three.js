import './favicon.ico';
import './css/boots.scss';
import './js/index.js';

import React from 'react';
import { ReactDOM, render } from 'react-dom';
import { BrowserRouter, Route, Redirect, Switch, NavLink } from 'react-router-dom';

const MainMenu = () => (
  <header className="mainmenu">
    <h1>React Router 4 App</h1>
    <ul>
      <li><NavLink to={'/'}>Home</NavLink></li>
      <li><NavLink activeClassName={"active"} to={'/about'}>About</NavLink></li>
      <li><NavLink activeClassName={"active"} to={'/users'}>Browse Users</NavLink></li>
      <li><NavLink activeClassName={"active"} to={'/products'}>Browse Products</NavLink></li>
    </ul>
  </header>
);

const UserNav = () => (
  <header className="submenu">
    <ul>
      <li><NavLink activeClassName={"active"} to={'/users/Moe'}>Moe</NavLink></li>
      <li><NavLink activeClassName={"active"} to={'/users/Larry'}>Larry</NavLink></li>
      <li><NavLink activeClassName={"active"} to={'/users/Curly'}>Curly</NavLink></li>
    </ul>
  </header>
);

const DNEPage = () => <div><h2>404: Not Found</h2></div>;
const HomePage = () => <div><h2>Home Page</h2></div>;
const AboutPage = () => <div><h2>About Page</h2></div>;

const BrowseUsersPage = () => (
  <div className="no-name">
    <h2>Users Page</h2>
    <div className="also-no-name">
      <BrowseUserTable />
    </div>
  </div>
);

const BrowseUserTable = () => (
  <table className="usertable table table-striped">
    <thead>
      <tr><th scope="col">ID</th><th scope="col">Name</th><th scope="col">Link</th></tr>
    </thead>
    <tbody>
      <tr><th scope="row">1</th><td>Moe</td><td><NavLink to={'/users/Moe'}>Link</NavLink></td></tr>
      <tr><th scope="row">2</th><td>Larry</td><td><NavLink to={'/users/Larry'}>Link</NavLink></td></tr>
      <tr><th scope="row">3</th><td>Curly</td><td><NavLink to={'/users/Curly'}>Link</NavLink></td></tr>
    </tbody>
  </table>
);

const BrowseProductsPage = () => (
  <div className="no-name">
    <h2>Products Page</h2>
    <div className="also-no-name">
      <BrowseProductTable />
    </div>
  </div>
);

const BrowseProductTable = () => (
  <table className="producttable table table-striped">
    <thead>
      <tr><th scope="col">ID</th><th scope="col">Name</th><th scope="col">Link</th></tr>
    </thead>
    <tbody>
      <tr><th scope="row">1</th><td>Face Slap</td><td><NavLink to={'/products/Slap'}>Link</NavLink></td></tr>
      <tr><th scope="row">2</th><td>Double Eye Poke</td><td><NavLink to={'/products/Poke'}>Link</NavLink></td></tr>
      <tr><th scope="row">3</th><td>Pie to the Face</td><td><NavLink to={'/products/Pie'}>Link</NavLink></td></tr>
    </tbody>
  </table>
);

const UserProfilePage = (props) => {
  // console.log( 'UserProfilePage' , props );
  
  // console.log( 'UserProfilePage' , props.history );
  // console.log( 'UserProfilePage' , props.location );
  // console.log( 'UserProfilePage' , props.staticContext ); // undefined

  // console.log( 'UserProfilePage' , props.match );
  // console.log( 'UserProfilePage' , props.match.isExact ); // true 
  // console.log( 'UserProfilePage' , props.match.params ); // Object { id: "Curly" }
  // console.log( 'UserProfilePage' , props.match.path ); // "/users/:id"
  // console.log( 'UserProfilePage' , props.match.url ); // "/users/Curly"
  
  return (
    <div className="no-name">
      <div className="also-no-name">
        <UserProfile userId={props.match.params.id}/>
      </div>
    </div>
  );
};

const UserProfile = ( {userId} ) => (
  <div>
    <h2>User Page</h2>
    <p className="lead">User ID: {userId}</p>
  </div>
);

const ProductProfilePage = (props) => {
  // console.log( 'ProductProfilePage' , props );

  // console.log( 'ProductProfilePage' , props.history );
  // console.log( 'ProductProfilePage' , props.location );
  // console.log( 'ProductProfilePage' , props.staticContext ); // undefined

  // console.log( 'ProductProfilePage' , props.match );
  // console.log( 'ProductProfilePage' , props.match.isExact ); // true
  // console.log( 'ProductProfilePage' , props.match.params ); // Object { id: "Slap" }
  // console.log( 'ProductProfilePage' , props.match.path ); // /products/:id
  // console.log( 'ProductProfilePage' , props.match.url ); // /products/Slap

  return (
    <div className="no-name">
      <div className="also-no-name">
        <ProductProfile productId={props.match.params.id}/>
      </div>
    </div>
  );
};

const ProductProfile = ( {productId} ) => (
  <div>
    <h2>Product Page</h2>
    <p className="lead">Product ID: {productId}</p>
  </div>
);

const PrimaryLayout = (props) => {
  // console.log( 'PrimaryLayout' , props );
  // console.log( 'PrimaryLayout' , props.children );
  // console.log( 'PrimaryLayout' , props.children._owner );
  // console.log( 'PrimaryLayout' , props.children._self );
  // console.log( 'PrimaryLayout' , props.children._source );
  // console.log( 'PrimaryLayout' , props.children._store ); 
  // console.log( 'PrimaryLayout' , props.children.key ); // null
  // console.log( 'PrimaryLayout' , props.children.props );
  // console.log( 'PrimaryLayout' , props.children.ref ); // null
  // console.log( 'PrimaryLayout' , props.children.type );
  
  return (
    <div className='primary-layout'>
      <MainMenu/>
      <main>
        {props.children}
      </main>
    </div>
  );
};

const UserSubLayout = () => (
  <div className="user-sub-layout">
    <aside>
      <UserNav />
    </aside>
    <div className="primary-content">
      <UserDispatch/>
    </div>
  </div>
);

const UserDispatch = () => (
  <Switch>
    <Route path="/users" exact component={BrowseUsersPage} />
    <Route path="/users/:id" component={UserProfilePage} />
  </Switch>
);

const ProductSubLayout = () => (
  <div className="product-sub-layout">
    <aside>
      <UserNav />
    </aside>
    <div className="primary-content">
      <ProductDispatch/>
    </div>
  </div>
);

const ProductDispatch = () => (
  <Switch>
    <Route path="/products" exact component={BrowseProductsPage} />
    <Route path="/products/:id" component={ProductProfilePage} />
  </Switch>
);

const App = () => (
  <BrowserRouter>
    <PrimaryLayout>
      <Switch>
        <Route path="/" exact component={HomePage} />
        <Route path="/about" component={AboutPage} />

        <Route path="/users" component={UserSubLayout} />
        {/*<Route path="/users" exact component={BrowseUsersPage} />
        <Route path="/users/:id" component={UserProfilePage} /> */}

        <Route path="/products" component={ProductSubLayout} />
        {/*<Route path="/products" exact component={BrowseProductsPage} />
        <Route path="/products/:id" component={ProductProfilePage} />*/}

        {/* <Route component={DNEPage} /> */}
        <Redirect to="/" />
      </Switch>
    </PrimaryLayout>
  </BrowserRouter>
);

render(<App />, document.getElementById('leApp'));